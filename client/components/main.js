import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Chart } from '/node_modules/chart.js/dist/Chart.js';
import { Gauge } from '/client/assets/gauge.min.js';
import '../layouts/main.html';

const POLL_INTERVAL = 4000;
const REST_URL = "https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,LTC&tsyms=USD,EUR";
const COLLECTION_NAME = "CryptoValue";

const IDBTC = "wPDnyNDCDcAsE7ELr";
const IDETH = "TqyrByxL9WzBQPHNX";
const IDLTC = "EL68EtNhPPQXs38Hy";

let switchBTC = "off";
let switchETH = "off";
let switchLTC = "off";
let switchChart = "off";
let switchJauge = "off";
let switchKpi = "off";

let vBTCevo, vETHevo, vLTCevo;
let oldBTC, oldETH, oldLTC;

Template.serverStatus.helpers({
    status:function(){
        let stat = Meteor.status();
        let classText = "";
        let messageText = "";
        if(stat.connected){
            classText = "success";
        }else{
            classText = "danger";
        }
        messageText = stat.status;
        return{
            "class" : classText,
            "text" : messageText
        };
    }
});

Template.currencies.helpers({
    value:function(){
        let btc = CRYPTONOW.findOne({_id:IDBTC});
        let eth = CRYPTONOW.findOne({_id:IDETH});
        let ltc = CRYPTONOW.findOne({_id:IDLTC});

        if(oldBTC == null || oldETH == null|| oldLTC == null){
            oldBTC = 0;
            oldETH = 0;
            oldLTC = 0;
        }

        return{
            vBTC : btc.value,
            vETH : eth.value,
            vLTC : ltc.value,
            evoBTC : vBTCevo,
            evoETH : vETHevo,
            evoLTC : vLTCevo,
            dBTC : precisionRound(btc.value - oldBTC.value,2),
            dETH : precisionRound(eth.value - oldETH.value,2),
            dLTC : precisionRound(ltc.value - oldLTC.value,2)
        }
    }
});

Template.addChart.helpers({
    switch:function(){
        return{
            switchBTC : switchBTC,
            switchETH : switchETH,
            switchLTC : switchLTC,
            switchChart : switchChart,
            switchJauge : switchJauge,
            switchKpi : switchKpi
        }
    }
});

Template.addChart.events({
    "click #addChartBtn":function(event){
        $("#chartsContainer").append("<div class=\"row\">\n" +
            "        <div class=\"col-lg-10\">\n" +
            "            <div class=\"card\">\n" +
            "               <canvas class=\"chart\" id='chart1'></canvas>\n" +
            "            </div>\n" +
            "        </div>\n" +
            "        <div class=\"col-lg-2\">\n" +
            "            <div class=\"card\">\n" +
            "                &times;\n" +
            "            </div>\n" +
            "        </div>\n" +
            "    </div>");
        generateChart();
        uncheckAll();
    },
    "click #selectBTC":function(event){
        switchCurrencies("BTC");
    },
    "click #selectETH":function(event){
        switchCurrencies("ETH");
    },
    "click #selectLTC":function(event){
        switchCurrencies("LTC");
    },
    "click #selectChart":function(event){
        switchChartType("chart");
    },
    "click #selectJauge":function(event){
        switchChartType("jauge");
    },
    "click #selectKPI":function(event){
        switchChartType("kpi");
    }
});

Meteor.startup(() => {
  if(Meteor.isClient){
      console.log("STARTUP CLIENT");
  }
  Meteor.subscribe(COLLECTION_NAME);

    const poll = () => {

        $.ajax({
            type: "POST",
            url: REST_URL
        }).done(function (data) {

            oldBTC = CRYPTONOW.findOne({_id:IDBTC});
            oldETH = CRYPTONOW.findOne({_id:IDETH});
            oldLTC = CRYPTONOW.findOne({_id:IDLTC});

            CRYPTONOW.update({_id:IDBTC},{value:data.BTC.EUR});
            CRYPTONOW.update({_id:IDETH},{value:data.ETH.EUR});
            CRYPTONOW.update({_id:IDLTC},{value:data.LTC.EUR});

            if(oldBTC.value < data.BTC.EUR){
                vBTCevo = "up";
            }
            if(oldBTC.value > data.BTC.EUR){
                vBTCevo = "down";
            }
            if(oldBTC.value == data.BTC.EUR){
                vBTCevo = "stay";
            }

            if(oldETH.value < data.ETH.EUR){
                vETHevo = "up";
            }
            if(oldETH.value > data.ETH.EUR){
                vETHevo = "down";
            }
            if(oldETH.value == data.ETH.EUR){
                vETHevo = "stay";
            }

            if(oldLTC.value < data.LTC.EUR){
                vLTCevo = "up";
            }
            if(oldLTC.value > data.LTC.EUR){
                vLBTCevo = "down";
            }
            if(oldLTC.value == data.LTC.EUR){
                vLTCevo = "stay";
            }
        });
    };
    poll();
    const interval = Meteor.setInterval(poll, POLL_INTERVAL);

    /*
    let ctx = document.getElementById('myChart').getContext('2d');
    let chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'doughnut',

    // The data for our dataset
    data: {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [{
        label: "My First dataset",
        backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850"],
        //borderColor: 'rgb(255, 99, 132)',
        data: [0, 10, 5, 2, 20, 30, 100 ],
      }]
    },

    // Configuration options go here
    options: {
      title: {
        display: true,
        text: 'Custom Chart Title',
        fontSize: 20
      },
      legend: {
        labels: {
          fontSize: 15
        }
      },
      tooltips: {
        bodyFontSize: 15
      }
    }
  });

    let ctx1 = document.getElementById('myChart2').getContext('2d');
    let chart1 = new Chart(ctx1, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [{
        label: "My First dataset",
        backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850"],
        //borderColor: 'rgb(255, 99, 132)',
        data: [0, 10, 5, 2, 20, 30, 20],
      }]
    },

    // Configuration options go here
    options: {
      title: {
        display: true,
        text: 'Custom Chart Title',
        fontSize: 20
      },
      legend: {
        labels: {
          fontSize: 15
        }
      },
      tooltips: {
        bodyFontSize: 15
      }
    }
  });


  new Chart(document.getElementById("line-chart"), {
    type: 'line',
    data: {
      labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
      datasets: [{ 
          data: [86,114,106,106,107,111,133,221,783,2478],
          label: "Africa",
          borderColor: "#3e95cd",
          fill: false
        }, { 
          data: [282,350,411,502,635,809,947,1402,3700,5267],
          label: "Asia",
          borderColor: "#8e5ea2",
          fill: false
        }, { 
          data: [168,170,178,190,203,276,408,547,675,734],
          label: "Europe",
          borderColor: "#3cba9f",
          fill: false
        }, { 
          data: [40,20,10,16,24,38,74,167,508,784],
          label: "Latin America",
          borderColor: "#e8c3b9",
          fill: false
        }, { 
          data: [6,3,2,2,7,26,82,172,312,433],
          label: "North America",
          borderColor: "#c45850",
          fill: false
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'World population per region (in millions)'
      }
    }
  });

    let opts = {
    angle: 0.15, // The span of the gauge arc
    lineWidth: 0.44, // The line thickness
    radiusScale: 1, // Relative radius
    pointer: {
      length: 0.51, // // Relative to gauge radius
      strokeWidth: 0.035, // The thickness
      color: 'black' // Fill color
    },
    staticLabels: {
      font: "1em sans-serif",  // Specifies font
      labels: [0, 15000, 30000],  // Print labels at these values
      color: "#000000",  // Optional: Label text color
      fractionDigits: 0  // Optional: Numerical precision. 0=round off.
    },
    limitMax: false,     // If false, max value increases automatically if value > maxValue
    limitMin: false,     // If true, the min value of the gauge will be fixed
    colorStart: '#6FADCF',   // Colors
    colorStop: '#8FC0DA',    // just experiment with them
    strokeColor: '#E0E0E0',  // to see which ones work best for you
    generateGradient: true,
    highDpiSupport: true,     // High resolution support

  };
  let target = document.getElementById('foo'); // your canvas element
  let gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
  gauge.maxValue = 30000; // set max gauge value
  gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
  gauge.animationSpeed = 50; // set animation speed (32 is default value)
  gauge.set(15000); // set actual value
  */
});

function precisionRound(number, precision) {
    var factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
}

function uncheckAll(){
    switchBTC = "off";
    switchETH = "off";
    switchLTC = "off";
    switchChart = "off";
    switchJauge = "off";
    switchKpi = "off";
    $("#selectJauge").removeClass("on");
    $("#selectKPI").removeClass("on");
    $("#selectChart").removeClass("on");
    $("#selectBTC").removeClass("on");
    $("#selectETH").removeClass("on");
    $("#selectLTC").removeClass("on");
}

function switchCurrencies(currencie){
    switch (currencie) {
        case 'BTC':
            if (switchBTC == "on"){
                switchBTC = "off";
                $("#selectBTC").removeClass("on");
            }else{
                switchBTC = "on";
                $("#selectBTC").addClass("on");
            }
            break;
        case 'ETH':
            if (switchETH == "on"){
                switchETH = "off";
                $("#selectETH").removeClass("on");
            }else{
                switchETH = "on";
                $("#selectETH").addClass("on");
            }
            break;
        case 'LTC':
            if (switchLTC == "on"){
                switchLTC = "off";
                $("#selectLTC").removeClass("on");
            }else{
                switchLTC = "on";
                $("#selectLTC").addClass("on");
            }
            break;
        default:
            console.log('Bad currencie');
    }
    console.log("currencies are :" + switchBTC + "," + switchETH + "," + switchLTC);
}

function switchChartType(chartType){
    switch (chartType) {
        case 'chart':
            if (switchChart == "on"){
                switchChart = "off";
                $("#selectChart").removeClass("on");
            }else{
                switchChart = "on";
                switchJauge = "off";
                switchKpi = "off";
                $("#selectChart").addClass("on");
                $("#selectKPI").removeClass("on");
                $("#selectJauge").removeClass("on");
            }
            break;
        case 'jauge':
            if (switchJauge == "on"){
                switchJauge = "off";
                $("#selectJauge").removeClass("on");
            }else{
                switchJauge = "on";
                switchChart = "off";
                switchKpi = "off";
                $("#selectJauge").addClass("on");
                $("#selectKPI").removeClass("on");
                $("#selectChart").removeClass("on");
            }
            break;
        case 'kpi':
            if (switchKpi == "on"){
                switchKpi = "off";
                $("#selectKPI").removeClass("on");
            }else{
                switchKpi = "on";
                switchJauge = "off";
                switchChart = "off";
                $("#selectKPI").addClass("on");
                $("#selectChart").removeClass("on");
                $("#selectJauge").removeClass("on");
            }
            break;
        default:
            console.log('Bad chart type');
    }
    console.log("charts are :" + switchChart + "," + switchJauge + "," + switchKpi);
}

function generateChart(){
    let ctx = document.getElementById('chart1').getContext('2d');
    ctx.height=500;
    let chart1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
            datasets: [{
                data: [86,114,106,106,107,111,133,221,783,2478],
                label: "Africa",
                borderColor: "#3e95cd",
                fill: false
            }, {
                data: [282,350,411,502,635,809,947,1402,3700,5267],
                label: "Asia",
                borderColor: "#8e5ea2",
                fill: false
            }, {
                data: [168,170,178,190,203,276,408,547,675,734],
                label: "Europe",
                borderColor: "#3cba9f",
                fill: false
            }, {
                data: [40,20,10,16,24,38,74,167,508,784],
                label: "Latin America",
                borderColor: "#e8c3b9",
                fill: false
            }, {
                data: [6,3,2,2,7,26,82,172,312,433],
                label: "North America",
                borderColor: "#c45850",
                fill: false
            }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'World population per region (in millions)'
            },
            responsive: true,
            maintainAspectRatio: false
        }
    });
}